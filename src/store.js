import { Router }                                 from 'react-native-router-flux';
import { createStore, applyMiddleware, compose }  from 'redux';
import { connect }                                from 'react-redux';
import createSagaMiddleware                       from "redux-saga";
import createLogger                               from "redux-logger";
import reducers                                   from './reducers';
import rootSaga                                   from './sagas';

const RouterWithRedux = connect()(Router);

const sagaMiddleware = createSagaMiddleware()

const loggerMiddleware = createLogger({ predicate: () => '__DEV__' });

const middleware = [sagaMiddleware, loggerMiddleware];

const store = compose(applyMiddleware(...middleware))(createStore)(reducers);

sagaMiddleware.run(rootSaga);

export default store;