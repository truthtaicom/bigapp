import {
  View,
  Text,
  StyleSheet,
  Dimensions,
}                                       from 'react-native';
import React, { Component, PropTypes }  from 'react';
import { bindActionCreators }           from 'redux';
import { connect }                      from 'react-redux';
// import { Actions, ActionConst }         from "react-native-router-flux";
import Icon                             from 'react-native-vector-icons/FontAwesome';
import { userLoginRequest }             from '../actions';
import { STYLE }                        from '../utils/variables';
import Spinner                          from 'react-native-loading-spinner-overlay';
import { LoginButton, AccessToken }     from 'react-native-fbsdk';

const {width, height} = Dimensions.get('window');

class LoginFB extends Component {
  render() {
    const { userLoginRequest, user } = this.props;
    return(
       <View style={styles.container}>
        <Spinner visible={user.userLogging}/>
        {
          user.userLogged &&
          <Text style={styles.logged}>
            User logged
          </Text>
        }
        <LoginButton
          publishPermissions={["publish_actions"]}
          onLoginFinished={
            (error, result) => {
              if (error) {
                alert("login has error: " + result.error);
              } else if (result.isCancelled) {
                alert("login is cancelled.");
              } else {
                AccessToken.getCurrentAccessToken().then(
                  (data) => userLoginRequest(data.accessToken.toString())
                )
              }
            }
          }
          onLogoutFinished={() => alert("logout.") }/>

      </View>
    )
  }
}

LoginFB.propTypes = {
    userLoginRequest: PropTypes.func,
    userLogging: PropTypes.bool,
    user: PropTypes.object
}

const stateToProps = (state) => {
  return {
      user: state.userReducer
  }
}

const dispatchToProps = (dispatch) => {
  return bindActionCreators({ userLoginRequest }, dispatch)
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    logged: {
      marginVertical: 20
    }
})

export default connect(stateToProps, dispatchToProps)(LoginFB)