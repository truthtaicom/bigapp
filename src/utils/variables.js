const STYLE = {
    blue: '#0099cc',
    pink: '#FF1654',
    white: '#fff',
    green: '#4CAF50',
    greenDarker: '#0085b1',
    sea: '#B2DBBF',
    yellow: '#B2DBBF',
    header: '#0099cc',
    greyLight: '#eaf4fa'
}

export { STYLE }