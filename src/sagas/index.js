import { fork }          from 'redux-saga/effects';
import { watchLogin }    from './userSaga';

export default function* rootSaga() {
  yield [
    fork(watchLogin)
  ];
}