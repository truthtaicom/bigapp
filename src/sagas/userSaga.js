import React                                          from 'react';
import { take, put, call, fork, select }              from 'redux-saga/effects';
import { takeEvery }                                  from 'redux-saga';
import { userLogin }                                  from '../services';
import {
    USER_LOGIN_REQUEST,
    userLoginSuccess,
    userLoginFailure
  }                                                   from '../actions';

export function* login(action) {
  try {
    const data = yield call(userLogin, action.token);
    yield put(userLoginSuccess(data))
  
  } catch(error) {
    console.log('SAGA LOGIN ERR: ', error);
    yield put(userLoginFailure(error));
  
  }
}

export function* watchLogin() {
    yield* takeEvery(USER_LOGIN_REQUEST, login);
}