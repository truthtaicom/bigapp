import React                             from 'react';
import { Platform, StyleSheet }          from 'react-native';
import { Actions, Scene, Router, Modal } from 'react-native-router-flux';
import { connect }                       from 'react-redux';
import { Home }                          from './containers';
import { STYLE }                         from './utils/variables';

const RouterWithRedux = connect()(Router);
const backBtn = require('./assets/images/back_chevron.png');

class Routes extends React.Component {
  render () {
    return (
        <RouterWithRedux navigationBarStyle={styles.navigationBarStyle} titleStyle={styles.titleStyle}>
        <Scene key="modal" component={Modal}>
          <Scene key="root" backButtonImage={backBtn}>
                <Scene key="home" component={Home} hideNavBar title="Home" initial={true} type="replace" />
          </Scene>
        </Scene>
      </RouterWithRedux>
    );
  }
}

const styles = StyleSheet.create({
  navigationBarStyle: {
    backgroundColor: STYLE.blue
  },
  titleStyle: {
    color: STYLE.white
  }
});

export default Routes