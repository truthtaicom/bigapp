import {
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILURE,
} from '../actions';

const initialState = {
  userLogged: null,
  userLogging: false,
  userLoggingError: false
};

const initializeState = () => Object.assign({}, initialState);

export default function user(state = initializeState(), action = {}) {
  switch (action.type) {

    case USER_LOGIN_REQUEST:
      return { ...state, userLogged: null, userLogging: true, userLoggingError: false };

    case USER_LOGIN_SUCCESS:
      return { ...state, userLogged: action.user, userLogging: false };

    case USER_LOGIN_FAILURE:
      return { ...state, stopPointGettingError: action.error, userLogging: false };

    default:
      return state;
  }
}