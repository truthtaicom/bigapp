import { combineReducers } from 'redux';
import userReducer from './userReducer';
import routesReducer from './routesReducer';

const rootReducer = combineReducers({ routesReducer, userReducer });

export default rootReducer;