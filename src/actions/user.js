import { checkStatus, parseJSON, headers } 	from '../utils';
import { API } 	                            from '../utils/constants';

export const USER_LOGIN_REQUEST = 'USER_LOGIN_REQUEST';
export const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS';
export const USER_LOGIN_FAILURE = 'USER_LOGIN_FAILURE';

export const userLoginRequest = (token) => ({ type: USER_LOGIN_REQUEST, token })
export const userLoginSuccess = (user)  => ({ type: USER_LOGIN_SUCCESS, user })
export const userLoginFailure = (error) => ({ type: USER_LOGIN_FAILURE, error })