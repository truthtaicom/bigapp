import { AppRegistry } from 'react-native';
import Root from "./src/containers/root";

AppRegistry.registerComponent("BigPayApp", () => Root);